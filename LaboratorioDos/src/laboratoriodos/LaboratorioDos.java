/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriodos;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class LaboratorioDos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        1. Nueva cotizacion
//          Nombre, Total Paredes, Ventanas, Pintar, 
//          Tiempo, Costo. 
//        Datos de la pared
//          Datos de la ventana

        String titulo = "Pintor UTN - v.01";
        String menu = "1. Nueva cotización\n"
                + "2. Salir";

        String menu1 = "Desea agregar otra pared\n"
                + "1. SI\n"
                + "2. NO";

        String menu2 = "Desea agregar una/otra venta\n"
                + "1. SI\n"
                + "2. NO";

        String menu3 = "Tipo de Ventana\n"
                + "1. Rectangular/Cuadrada\n"
                + "2. Circular";

        while (true) {
            int op = Util.leerIntJOP(menu, titulo);
            if (op == 1) {
                Logica log = new Logica(Util.leerStringJOP("Cliente: ", titulo));
                int canP = 1;
                while (true) {
                    double largo = Util.leerDoublesJOP("Largo de la pared #" + canP, titulo);
                    double ancho = Util.leerDoublesJOP("Ancho de la pared #" + canP, titulo);
                    log.agregarPared(largo, ancho);
                    int canV = 1;
                    while (true) {
                        op = Util.leerIntJOP(menu2, titulo);
                        if (op == 2) {
                            break;
                        } else if (op == 1) {
                            op = Util.leerIntJOP(menu3, titulo);
                            if (op == 1) {
                                largo = Util.leerDoublesJOP("Largo de la ventana #" + canV, titulo);
                                ancho = Util.leerDoublesJOP("Ancho de la ventana #" + canV, titulo);
                                log.agregarVentana(largo, ancho);
                            } else {
                                ancho = Util.leerDoublesJOP("Ancho(diametro) de la ventana #" + canV, titulo);
                                log.agregarVentana(ancho);
                            }
                        }
                        canV++;
                    }
                    op = Util.leerIntJOP(menu1, titulo);
                    if (op == 2) {
                        break;
                    }
                    canP++;
                }
                Util.mostrarJOP(log.cotizar());
            } else if (op == 2) {
                Util.mostrarJOP("Gracias por utilizar la aplicación");
                break;
            }
        }

    }

}
