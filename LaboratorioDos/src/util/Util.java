/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class Util {

    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        int num = Integer.parseInt(sc.nextLine());
        return num;
    }

    public static int leerIntJOP(String mensaje, String titulo) {
        int num = Integer.parseInt(JOptionPane.showInputDialog(null, mensaje, titulo, JOptionPane.QUESTION_MESSAGE));
        return num;
    }

    public static double leerDoublesJOP(String mensaje, String titulo) {
        double num = Double.parseDouble(JOptionPane.showInputDialog(null, mensaje, titulo, JOptionPane.QUESTION_MESSAGE));
        return num;
    }

    public static String leerStringJOP(String mensaje, String titulo) {
        return JOptionPane.showInputDialog(null, mensaje, titulo, JOptionPane.QUESTION_MESSAGE);
    }

    public static void mostrarJOP(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje);
    }

    public static char leerChar(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        char letra = sc.nextLine().charAt(0);
        return letra;
    }
}
