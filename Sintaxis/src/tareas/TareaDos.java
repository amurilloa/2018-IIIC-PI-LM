/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareas;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class TareaDos {

    public static void main(String[] args) {
        OtroNumero on = null;
        String menu = "\nTarea Dos - v0.1\n"
                + "1. Digitar el número\n"
                + "2. Primo\\Compuesto\n"
                + "3. Tipo de número\n"
                + "4. Salir\n"
                + "Ingrese una opción";
        APP:
        do {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    int num = Util.leerInt("Digite un número");
                    on = new OtroNumero(num);
                    break;
                case 2:
                    if (on != null) {
                        if (on.esPrimo()) {
                            System.out.println(on.getNumero() + " es primo");
                        } else {
                            System.out.println(on.getNumero() + " es compuesto");
                        }
                    } else {
                        System.out.println("Es requerido digitar el número (opción 1)");
                    }
                    break;
                case 3:
                    if (on != null) {
                        switch (on.tipoNumero()) {
                            case 1:
                                System.out.println(on.getNumero() + " es un número perfecto");
                                break;
                            case 2:
                                System.out.println(on.getNumero() + " es un número deficiente");
                                break;
                            case 3:
                                System.out.println(on.getNumero() + " es un número abundante");
                                break;
                        }
                    } else {
                        System.out.println("Es requerido digitar el número (opción 1)");
                    }
                    break;
                case 4:
                    break APP;
                default:
                    System.out.println("Opción Inválida!!");
            }
        } while (true);

    }

}
