/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareas;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class Logica {

    public static String tareaUno(int can, int pre, int edad) {
        if (can > 0) {
            if (can == 1) {
                if (edad >= 18 && edad <= 30) {
                    double des = pre * 0.078;
                    double pf = pre - des;
                    String res = String.format("SubTotal: %d\n"
                            + "   Desc.: %.2f\n"
                            + "   Total: %.2f", pre, des, pf);
                    return res;
                } else if (edad > 30) {
                    double des = pre * 0.10;
                    double pf = pre - des;
                    String res = String.format("SubTotal: %d\n"
                            + "   Desc.: %.2f\n"
                            + "   Total: %.2f", pre, des, pf);
                    return res;
                } else {
                    String res = String.format("Total: %d", pre);
                    return res;
                }
            } else if (can == 2) {
                double des = pre * can * 0.115;
                double pf = pre * can - des;
                String res = String.format("SubTotal: %d\n"
                        + "   Desc.: %.2f\n"
                        + "   Total: %.2f", pre * can, des, pf);
                return res;
            } else if (can > 3) {
                double des = pre * can * 0.15;
                double pf = pre * can - des;
                String res = String.format("SubTotal: %d\n"
                        + "   Desc.: %.2f\n"
                        + "   Total: %.2f", pre * can, des, pf);
                return res;
            } else {
                String res = String.format("Total: %d", pre * can);
                return res;
            }
        } else {
            return "Cantidad de viajeros inválida!!";
        }

    }

}
