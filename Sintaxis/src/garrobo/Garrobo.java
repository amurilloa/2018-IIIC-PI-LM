/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garrobo;

/**
 *
 * @author ALLAN
 */
public class Garrobo {

    public String nombre;
    public double distancia;
    public double tiempo;

    public Garrobo() {
    }

    public Garrobo(String nombre, double distancia, double tiempo) {
        this.nombre = nombre;
        this.distancia = distancia;
        this.tiempo = tiempo;
    }

    /**
     * Calcula el tiempo que tarda un garrobo en recorrer una distancia
     * determinada
     *
     * @param distancia distancia a evaluar
     * @return tiempo en recorrer la distancia
     */
    public double calcularTiempo(double distancia) {
        double t = distancia / velocidad();
        return t;
    }

    /**
     * Calcula la distancia que puede recorrer el garrobo en un tiempo
     * determinado
     *
     * @param tiempo tiempo a evaluar
     * @return distancia que puede recorrer en el tiempo dado
     */
    public double calcularDistancia(double tiempo) {
        double d = velocidad() * tiempo;
        return d;

    }

    /**
     * Calcula la velocidad configurada para el garrobo 
     * @return velocidad del garrobo
     */
    private double velocidad() {
        return distancia / tiempo;
    }

}
