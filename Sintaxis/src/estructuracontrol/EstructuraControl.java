/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuracontrol;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author ALLAN
 */
public class EstructuraControl {

    /**
     * 
     * @param mensaje
     * @return 
     */
    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje);
        int num = Integer.parseInt(sc.nextLine());
        return num;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String menu
                = "Menu Principal\n"
                + "1. Ejercicio 1\n"
                + "2. Ejercicio 2\n"
                + "3. Ejercicio 3\n"
                + "4. Ejercicio 4\n"
                + "5. Ejercicio 5\n"
                + "6. Ejercicio 6\n"
                + "7. Salir\n"
                + "Seleccione una opción: ";
        int op = leerInt(menu);

        switch (op) {
            case 1:
                System.out.println("Ejercicio 1");
                int num = leerInt("Digite un número: ");
                System.out.println(Logica.ejercicioUno(num));
                break;
            case 2:
                System.out.println("Ejercicio 2");
                System.out.println("Número Aleatorio: " + Logica.ejercicioDos());
                break;
            case 3:
                System.out.println("Ejercicio 3");
                System.out.print("Cant. Pasajeros: ");
                int pa = Integer.parseInt(sc.nextLine());
                System.out.print("Cant. Ejes: ");
                int ej = Integer.parseInt(sc.nextLine());
                System.out.print("Precio: ");
                double pre = Double.parseDouble(sc.nextLine());
                System.out.println("El costo final del vehículo es: "
                        + Logica.ejercicioTres(pre, pa, ej));
                break;
            case 4:
                System.out.println("Ejercicio 4");
                System.out.print("Nota: ");
                int nota = Integer.parseInt(sc.nextLine());
                System.out.println(Logica.ejercicioCuatro(nota));
                break;
            case 5:
                System.out.println("Ejercicio 5");
                System.out.print("Canciones: ");
                int can = Integer.parseInt(sc.nextLine());
                System.out.print("Partituras: ");
                int par = Integer.parseInt(sc.nextLine());
                System.out.println(Logica.ejercicioCinco(par, can));
                break;
            case 6:
                System.out.println("Ejercicio 6");
                System.out.print("Monto: ");
                int monto = Integer.parseInt(sc.nextLine());
                System.out.println(Logica.ejercicioSeis(monto));
                break;
            case 7:
                System.out.println("Gracias por utilizar nuestra aplicación");
                break;
            default:
                System.out.println("Opción Inválida");
        }

    }
}
