/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuracontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EstructurasIterativas {

    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        int num = Integer.parseInt(sc.nextLine());
        return num;
    }

    public static void main(String[] args) {

        int tabla = leerInt("Digite la tabla deseada");
//        int contador = 1;
//        while (contador <= 10) {
//            String res = String.format("%dx%d=%d", tabla, contador, (tabla * contador));
//            System.out.println(res);
//            //System.out.println(tabla + "x" + contador + "=" + (tabla * contador));
//            contador++;
//        }
//        int contador = 1;
//        int sum = 0;
//        while (contador <= 10) {
//            sum += leerInt("#" + contador);
//            contador++;
//        }
//        System.out.println(sum);
//        
        for (int i = 1; i <= 10; i++) {
            System.out.println(tabla + "x" + i + "=" + (tabla * i));
            i++;
        }

        while (true) {
            int num = leerInt("#");
            if (num < 0) {
                System.out.println("Gracias por utilziar nuestra app");
                break;
            } else {
                System.out.println(num);
            }
        }

    }
}
