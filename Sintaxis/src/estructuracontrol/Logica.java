/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuracontrol;

/**
 *
 * @author ALLAN
 */
public class Logica {

    /**
     * Determinar sin un número es positivo o negativo y si es divisible entre 5
     *
     * @param num numero a evalar
     * @return Resultado de la evaluación
     */
    public static String ejercicioUno(int num) {
        String res = "";
        if (num >= 0) {
            res += "Es positivo";
        } else {
            res += "Es negativo";
        }

        if (num % 5 == 0) {
            res += " y es divisible entre 5";
        }
        return res;
    }

    public static int ejercicioDos() {
        int min = 1;
        int max = 6;
        int ale = (int) (Math.random() * max) + min;
        return ale;
    }

    public static double ejercicioTres(double pre, int pa, int ej) {
        double imp = pre * 0.01; //100
        double cT = pre + imp;

        if (pa < 20) {
            cT += imp * 0.01;
        } else if (pa >= 20 && pa <= 60) {
            cT += imp * 0.05; // 5
        } else {
            cT += imp * 0.08;
        }

        if (ej == 2) {
            cT += imp * 0.05; //5
        } else if (ej == 3) {
            cT += imp * 0.10;
        } else if (ej > 3) {
            cT += imp * 0.15;
        }

        return cT;

    }

    public static String ejercicioCuatro(int nota) {
//        if (nota >= 90) {
//            return "Sobresaliente";
//        } else if (nota >= 80) {
//            return "Notable";
//        } else if (nota >= 70) {
//            return "Bien";
//        } else {
//            return "Insuficiente";
//        }
        if (nota < 70) {
            return "Insuficiente";
        } else if (nota < 80) {
            return "Bien";
        } else if (nota < 90) {
            return "Notable";
        } else {
            return "Sobresaliente";
        }
    }

    public static String ejercicioCinco(int par, int can) {
        if (can > 10 && par > 5) {
            return "Músico Consagrado";
        } else if (can >= 7 && can <= 10) {
            if (par == 0) {
                return "Músico Naciente";
            } else if (par >= 1 && par <= 5) {
                return "Músico Estelar";
            } else {
                return "Músico en Formación";
            }
        } else {
            return "Músico en Formación";
        }
    }

    public static String ejercicioSeis(int monto) {
        String res = "";

        int moneda = 500;
        if (monto >= moneda) {
            int b = monto / moneda;
            monto %= moneda;
            res += String.format("%d billetes de %d\n", b, moneda);
        }

        moneda = 200;
        if (monto >= moneda) {
            int b = monto / moneda;
            monto %= moneda;
            res += String.format("%d billetes de %d\n", b, moneda);
        }

        moneda = 100;
        if (monto >= moneda) {
            int b = monto / moneda;
            monto %= moneda;
            res += String.format("%d billetes de %d\n", b, moneda);
        }

        moneda = 50;
        if (monto >= moneda) {
            int b = monto / moneda;
            monto %= moneda;
            res += String.format("%d billetes de %d\n", b, moneda);
        }
        moneda = 20;
        if (monto >= moneda) {
            int b = monto / moneda;
            monto %= moneda;
            res += String.format("%d billetes de %d\n", b, moneda);
        }
        moneda = 10;
        if (monto >= moneda) {
            int b = monto / moneda;
            monto %= moneda;
            res += String.format("%d billetes de %d\n", b, moneda);
        }
        moneda = 5;
        if (monto >= moneda) {
            int b = monto / moneda;
            monto %= moneda;
            res += String.format("%d billetes de %d\n", b, moneda);
        }

        moneda = 2;
        if (monto >= moneda) {
            int b = monto / moneda;
            monto %= moneda;
            res += String.format("%d monedas de %d\n", b, moneda);
        }

        if (monto > 0) {
            res += String.format("%d monedas de 1", monto);

        }
        return res;
    }
}

//        int a = 19;
//
//        if (a < 10) {
//            System.out.println("Menor a 10");
//        } else {
//            System.out.println("Mayor a 10");
//        }
//
//        int num = 2;//Integer.parseInt(JOptionPane
//        //.showInputDialog("Digite un número"));
//
//        if (num % 2 == 0) {
//            String res = String.format("El numero %d es par", num);
//            System.out.println(res);
//        } else {
//            System.out.println("El número " + num + " es impar");
//        }
//
//        int num1 = Integer.parseInt(JOptionPane
//                .showInputDialog("#1"));
//        int num2 = Integer.parseInt(JOptionPane
//                .showInputDialog("#2"));
//
//        if (num1 > num2) {
//            String res = String.format("%d es mayor que %d", num1, num2);
//            System.out.println(res);
//        } else if (num2 > num1) {
//            String res = String.format("%d es mayor que %d", num2, num1);
//            System.out.println(res);
//        } else {
//            String res = String.format("%d es igual a %d", num1, num2);
//            System.out.println(res);
//        }
