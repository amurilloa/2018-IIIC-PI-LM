
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class LaboratorioUno {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        Circunferencia moneda = new Circunferencia();
//        moneda.setRadio(1.4);
//        Circunferencia rueda = new Circunferencia(10.2);
//
//        System.out.println("El radio de la moneda: " + moneda.getRadio());
//        System.out.println("El radio de la rueda: " + rueda.getRadio());
//        System.out.println("El área de la moneda es: " + moneda.calcularArea());
//        System.out.println("El área de la rueda es: " + rueda.calcularArea());
//        System.out.println("El perímetro de la moneda es: " + moneda.calcularPerimetro());
//        System.out.println("El perímetro de la rueda es: " + rueda.calcularPerimetro());
//
//        Rectangulo pared = new Rectangulo(3, 8);//24
//        Rectangulo ventana = new Rectangulo(2, 2);//4
//        double aPintar = pared.calcularArea() - ventana.calcularArea();//20
//        int tiempo = (int) (aPintar * 10);//200
//        String res = String.format("El pintor tardará %d:%d en pintar %.2fm²",
//                (tiempo / 60), (tiempo % 60), aPintar);
//        System.out.println(res);
//        
//        int x = 002;
//        System.out.println(x);
//        Fecha fec1 = new Fecha();
//        Fecha fec2 = new Fecha(29,02,2000);
//        System.out.println(fec1.getFecha());
//        System.out.println(fec1.getFechaLetra());
//        System.out.println(fec2.getFecha());
//        System.out.println(fec2.getFechaLetra());
//        Articulo art = new Articulo(123, "Arroz", 1783, 3);
//        System.out.println(art.calcularIVA());
//        Temperatura temp = new Temperatura(23);
//        System.out.println(String.format("%.0f°C equivalen a %.0f°F",
//                temp.getGradosCel(), temp.convertirAFarenheit()));
//        CambioDeDivisas cd = new CambioDeDivisas(600);
//        cd.setColones(100000);
//        System.out.println(String.format("%.2f CRC --> %.2f USD",
//                cd.getColones(), cd.convertirADolares()));
//        Gasolinera gas = new Gasolinera();
//        gas.setGalones(3.42);
//        String str = String.format("Por %.2fgal debe pagar %.0f CRC",
//                gas.getGalones(),
//                gas.calcularCosto());
//        System.out.println(str);
        Mesa m1 = new Mesa();
        m1.capturarOrden(1, 3);//45
        m1.capturarOrden(4, 3);//24
        m1.capturarOrden(6, 1);//6
        System.out.println(m1.calcularCuenta());

    }

}
