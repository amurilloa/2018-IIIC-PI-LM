/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class Gasolinera {

    private double precio;
    private double galones;

    private final double LIT_X_GAL;

    public Gasolinera() {
        precio = 875;
        LIT_X_GAL = 3.78541;
    }

    public double calcularCosto() {
        return galones * LIT_X_GAL * precio;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getGalones() {
        return galones;
    }

    public void setGalones(double galones) {
        this.galones = galones;
    }

}
