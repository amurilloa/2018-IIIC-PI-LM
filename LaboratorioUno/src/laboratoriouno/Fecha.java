/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriouno;

/**
 *
 * @author ALLAN
 */
public class Fecha {

    private int dia;
    private int mes;
    private int anno;

    public Fecha() {
        dia = 1;
        mes = 1;
        anno = 2000;
    }

    public Fecha(int dia, int mes, int anno) {
        if (validarFecha(dia, mes, anno)) {
            this.dia = dia;
            this.mes = mes;
            this.anno = anno;
        } else {
            this.dia = 1;
            this.mes = 1;
            this.anno = 2000;
        }
    }

    /**
     * Modifica el valor de la fecha
     *
     * @param dia int dia
     * @param mes int mes
     * @param anno int anno
     */
    public void setFecha(int dia, int mes, int anno) {
        if (validarFecha(dia, mes, anno)) {
            this.dia = dia;
            this.mes = mes;
            this.anno = anno;
        } else {
            this.dia = 1;
            this.mes = 1;
            this.anno = 2000;
        }
    }

    /**
     * Obtiene la fecha en formato DD/MM/AAAA
     *
     * @return String con la fecha
     */
    public String getFecha() {
        String fecha = String.format("%02d/%02d/%04d", dia, mes, anno);
        return fecha;
    }

    public String getFechaLetra() {
        String fecha = String.format("%d de %s de %d", dia, convertir(mes), anno);
        return fecha;
    }

    /**
     * Convierte el número del mes al equivalente en texto
     *
     * @param mes int número del mes
     * @return string con el nombre del mes
     */
    private String convertir(int mes) {
        switch (mes) {
            case 1:
                return "Enero";
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Septiembre";
            case 10:
                return "Octubre";
            case 11:
                return "Noviembre";
            default:
                return "Diciembre";
        }

    }

    private boolean validarFecha(int dia, int mes, int anno) {
        if (mes > 12 || dia > 31 || mes < 1 || dia < 1) {
            return false;
        }

        if (dia > 30 && (mes == 4 || mes == 6 || mes == 9 || mes == 11)) {
            return false;
        }

        if (mes == 2 && esBisiesto(anno) && dia > 29) {
            return false;
        }

        if (mes == 2 && !esBisiesto(anno) && dia > 28) {
            return false;
        }

        return true;
    }

    /**
     * Determina sin un año es bisiesto
     *
     * @param anno int año
     * @return true si es bisiesto
     */
    private boolean esBisiesto(int anno) {
        if (anno % 4 == 0) {
            if (anno % 100 == 0) {
                return anno % 400 == 0;
            }
            return true;
        }
        return false;
    }
}
