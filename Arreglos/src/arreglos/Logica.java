/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private Perro[] perros;

    public Logica() {
        perros = new Perro[25];
    }

    public void capturarPerro(Perro perro) {
        for (int i = 0; i < perros.length; i++) {
            if (perros[i] == null) {
                perros[i] = perro;
                break;
            }
        }
    }

    public String reporte() {
        String txt = "";
        for (Perro perro : perros) {
            if (perro != null) {
                txt += perro.info() + "\n";
            }
        }
        return txt;
    }

    public void guardarPerros() {

    }

    public void cargarPerros() {

    }

    public Perro buscarPerro(String nom, String raz) {
        for (Perro perro : perros) {
            if (perro != null && perro.getNombre().equals(nom)
                    && perro.getRaza().equals(raz)) {
                return perro;
            }
        }
        return null;
    }

}
