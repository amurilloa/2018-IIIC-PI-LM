/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encuesta;

/**
 *
 * @author ALLAN
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();
        log.agregarFormulario(new Formulario(111, 1, 2, 0));
        log.agregarFormulario(new Formulario(112, 2, 1, 100000));
        log.agregarFormulario(new Formulario(113, 1, 2, 0));
        log.agregarFormulario(new Formulario(114, 1, 2, 0));
        log.agregarFormulario(new Formulario(115, 2, 1, 100000));
        log.agregarFormulario(new Formulario(116, 1, 1, 100000));
        log.agregarFormulario(new Formulario(117, 2, 2, 0));
        log.agregarFormulario(new Formulario(118, 2, 2, 0));
        log.agregarFormulario(new Formulario(119, 1, 1, 100000));
        log.agregarFormulario(new Formulario(120, 2, 1, 100000));
        log.agregarFormulario(new Formulario(121, 2, 1, 100000));
        log.agregarFormulario(new Formulario(122, 1, 2, 0));
        log.agregarFormulario(new Formulario(123, 2, 2, 0));
        log.agregarFormulario(new Formulario(124, 2, 1, 100000));
        log.agregarFormulario(new Formulario(125, 1, 2, 0));

        System.out.println(String.format("Porcentaje de Hombres: %.0f%s",
                log.porcentajeHombres(), "%"));
    }
}
