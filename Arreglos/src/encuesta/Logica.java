/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encuesta;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private Formulario[] encuesta;

    public Logica() {
        encuesta = new Formulario[50];
    }

    public void agregarFormulario(Formulario form) {
        for (int i = 0; i < encuesta.length; i++) {
            if (encuesta[i] == null) {
                encuesta[i] = form;
                break;
            }
        }
    }

    public double porcentajeHombres() {
        int hom = 0;
        int tot = 0;
        for (int i = 0; i < encuesta.length; i++) {
            if (encuesta[i] != null) {
                tot++;
                if (encuesta[i].getSexo() == 1) {
                    hom++;
                }
            }
        }
        return hom * 100.0 / tot;
    }

}
