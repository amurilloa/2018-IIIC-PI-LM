/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cronometro;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class Cronometro {

    private int hor;
    private int min;
    private int seg;
    private int ms;
    private Color color;
    private Color colorB;
    private boolean corriendo;
    private final int X = 30;
    private final int Y = 300;
    private LinkedList<String> tiempos;

    public Cronometro() {
        color = Color.GREEN;
        colorB = Color.GREEN;
        tiempos = new LinkedList<>();
    }

    public void dibujarNumero(Graphics g, int num, int x, int y) {
        g.setColor(color);
        if (num != 1 && num != 2 && num != 3 && num != 7) {
            g.fillRect(x, y + 10, 10, 80);//sup izq
        }
        if (num != 1 && num != 4) {
            g.fillRect(x + 10, y, 80, 10);//sup
        }
        if (num != 5 && num != 6) {

            g.fillRect(x + 90, y + 10, 10, 80);//sup der
        }
        if (num != 1 && num != 0 && num != 7) {
            g.fillRect(x + 10, y + 90, 80, 10);//cen
        }

        if (num == 2 || num == 6 || num == 8 || num == 0) {
            g.fillRect(x, y + 100, 10, 80);//inf izq
        }

        if (num != 2) {
            g.fillRect(x + 90, y + 100, 10, 80);//inf der
        }
        if (num != 1 && num != 4 && num != 7) {
            g.fillRect(x + 10, y + 180, 80, 10);//inf
        }
    }

    public void pintar(Graphics g) {
        int u = hor % 10;
        int d = hor / 10;

        g.setColor(colorB);
        g.drawOval(50, 50, 900, 900);
        
        int angulo = 6 * seg;
        g.fillArc(50, 50, 900, 900, 90, -angulo);
        g.setColor(Color.BLACK);
        g.fillArc(60, 60, 880, 880, 90, -angulo);

        dibujarNumero(g, d, X + 100, Y + 100);
        dibujarNumero(g, u, X + 220, Y + 100);

        g.fillRect(X + 333, Y + 160, 15, 15);
        g.fillRect(X + 333, Y + 215, 15, 15);

        u = min % 10;
        d = min / 10;

        dibujarNumero(g, d, X + 360, Y + 100);
        dibujarNumero(g, u, X + 480, Y + 100);

        g.fillRect(X + 593, Y + 160, 15, 15);
        g.fillRect(X + 593, Y + 215, 15, 15);

        u = seg % 10;
        d = seg / 10;

        dibujarNumero(g, d, X + 620, Y + 100);
        dibujarNumero(g, u, X + 740, Y + 100);

        g.setFont(new Font("Arial", Font.BOLD, 45));

        int y = 355;
        int i = 1;
        for (String tiempo : tiempos) {
            g.drawString((i++) + " ~ " + tiempo, X + 330, Y + y);
            y += 55;
        }
    }

    public void incrementar(int mil) {
        if (corriendo) {
            ms += mil;
        }
        int repMS = ms;
        hor = repMS / 1000 / 60 / 60;
        repMS -= (hor * 60 * 60 * 1000);
        min = repMS / 1000 / 60;
        repMS -= (min * 60 * 1000);
        seg = repMS / 1000;
    }

    public void iniciar() {
        corriendo = true;
    }

    public void detener() {
        corriendo = false;
    }

    public void reiniciar() {
        if (!corriendo) {
            ms = 0;
            tiempos.clear();
        }
    }

    public void cambiarBorde(int keyCode) {
        int r = (int) (Math.random() * 255);
        int g = (int) (Math.random() * 255);
        int b = (int) (Math.random() * 255);
        colorB = new Color(r, g, b);
    }

    public void parcial() {
        if (corriendo) {
            String h = String.format("%02d", hor);
            String m = String.format("%02d", min);
            String s = String.format("%02d", seg);
            if (tiempos.size() >= 5) {
                tiempos.removeFirst();
            }
            tiempos.add(h + ":" + m + ":" + s);
        }
    }

}
