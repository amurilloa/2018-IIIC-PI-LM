/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cronometro;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class MiPanel extends JPanel implements KeyListener {

    private Cronometro cro;

    public MiPanel() {
        cro = new Cronometro();
        addKeyListener(this);
        setFocusable(true);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getHeight());
        cro.incrementar(200);
        cro.pintar(g);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(1000, 1000);
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_I) {
            cro.iniciar();
        } else if (ke.getKeyCode() == KeyEvent.VK_S) {
            cro.detener();
        } else if (ke.getKeyCode() == KeyEvent.VK_R) {
            cro.reiniciar();
        } else if (ke.getKeyCode() == KeyEvent.VK_P) {
            cro.parcial();
        } else {
            cro.cambiarBorde(ke.getKeyCode());
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
