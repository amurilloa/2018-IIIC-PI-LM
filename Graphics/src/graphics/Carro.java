/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Carro {

    private int placa;
    private boolean encendido;
    private Color color;
    private int x;
    private int y;

    public Carro(int placa, Color color, int x, int y) {
        this.placa = placa;
        this.color = color;
        this.x = x;
        this.y = y;
    }

    public void pintar(Graphics g) {
        g.setColor(color);
        int[] xs = {x + 30, x + 60, x + 90, x + 150, x + 190, x + 230, x + 230, x + 200, x + 30};
        int[] ys = {y + 30, y + 30, y + 0, y + 0, y + 30, y + 35, y + 55, y + 60, y + 60};
        g.fillPolygon(xs, ys, xs.length);
        if (color == Color.BLACK) {
            g.setColor(Color.LIGHT_GRAY);
        } else {
            g.setColor(Color.BLACK);
        }
        g.fillOval(x + 50, y + 40, 35, 35);
        g.fillOval(x + 165, y + 40, 35, 35);

        if (encendido) {
            g.setColor(Color.LIGHT_GRAY);
            g.fillOval(x + 20, y + 55, 20, 20);
            g.fillOval(x + 10, y + 50, 20, 20);
            g.fillOval(x + 10, y + 40, 20, 20);
            g.setColor(Color.YELLOW);
            g.fillRect(x + 221, y + 36, 8, 8);
        }
    }

    public int getPlaca() {
        return placa;
    }

    public void setPlaca(int placa) {
        this.placa = placa;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isEncendido() {
        return encendido;
    }

    public void setEncendido(boolean encendido) {
        this.encendido = encendido;
    }
}
