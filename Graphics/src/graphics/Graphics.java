/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import javax.swing.JFrame;

/**
 *
 * @author ALLAN
 */
public class Graphics {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        //Crear un formulario
        JFrame frm = new JFrame("DemoGraphics v.01");
        //Asignar la función de cierre del formulario
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Agregar un panel (propio) al formulario
        frm.add(new MiPanel());
        //Validar Interfaz
        frm.pack();
        //Asignar dimensiones
        frm.setSize(800, 1000);
        //Centrar la ventana 
        frm.setLocationRelativeTo(null);
        //Opcional - Evitar que cambie de tamaño - Extra
        //frm.setResizable(false);
        //Hacer visible el formulario
        frm.setVisible(true);
        while (true) {
            frm.repaint();
            Thread.sleep(15);
        }
    }

}
