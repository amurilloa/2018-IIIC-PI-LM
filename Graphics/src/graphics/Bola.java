/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Bola {

    private Color color;
    private int x;
    private int y;
    private int dir;

    public Bola(Color color, int x, int y) {
        this.color = color;
        this.x = x;
        this.y = y;
    }

    public void pintar(Graphics g) {
        g.setColor(Color.BLUE);
        g.fillOval(x, y, 100, 100);
        g.setColor(color);
        g.fillOval(x, y, 90, 90);
    }

    public void mover() {
        if (dir == 0) {
            y += 5;
        } else if (dir == 1) {
            y -= 5;
        } else if (dir == 2) {
            x += 5;
        } else if (dir == 3) {
            x -= 5;
        } else if (dir == 4) {
            x += 5;
            y += 5;
        }
    }

    public void rebotar(int limX, int limY) {
        if (y >= limY - 100) {
            dir = 1;
        } else if (y <= 0) {
            dir = 4;
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

}
