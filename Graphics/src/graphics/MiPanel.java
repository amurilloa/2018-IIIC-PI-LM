/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class MiPanel extends JPanel {

    private Carro c1;
    private Carro c2;
    private Bola bolita;

    public MiPanel() {
        c1 = new Carro(1, Color.BLUE, 0, 100);
        c1.setEncendido(true);
        c2 = new Carro(2, Color.BLACK, 100, 150);
        c2.setEncendido(true);
        bolita = new Bola(Color.YELLOW, 100, 100);

    }

    //https://www.youtube.com/watch?v=zq9a6D7qmP4
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        //Fondo
        g.setColor(Color.white);
        g.fillRect(0, 0, 800, 1000);
        bolita.pintar(g);
        bolita.mover();
        bolita.rebotar(getWidth(), getHeight());
        
//        g.drawLine(100, 100, 100, 300);
//        g.drawLine(101, 100, 101, 300);
//        g.drawLine(102, 100, 102, 300);
//        g.fillRect(200, 200, 200, 3);
//        g.setColor(new Color(0, 0, 0));
//        g.fillOval(410, 510, 150, 150);
//        g.drawRoundRect(WIDTH, WIDTH, WIDTH, WIDTH, WIDTH, WIDTH);
//
//        g.setFont(new Font("Arial", Font.BOLD, 100));
//        g.drawString("1", 200, 250);
//        g.drawString("2", 600, 250);
//        g.drawString("3", 200, 750);
//        g.drawString("4", 600, 750);
//
//        g.fillArc(50, 550, 200, 200, 90, -90);
//
//        g.setColor(Color.red);
//        g.drawLine(400, 0, 400, 1000);
//        g.drawLine(0, 500, 800, 500);
//
//        int[] xs = {200, 300, 280, 180};
//        int[] ys = {200, 300, 320, 220};
//        g.fillPolygon(xs, ys, xs.length);

        //Fondo
//        g.setColor(Color.white);
//        g.fillRect(0, 0, 800, 1000);
//
//        //Circulo de abajo
//        g.setColor(Color.black);
//        g.fillOval(250, 500, 300, 300);
//        g.setColor(Color.white);
//        g.fillOval(254, 504, 292, 292);
//        g.setColor(Color.black);
//        g.setColor(Color.white);
//        g.fillArc(255, 700, 300, 200, 0, 180);
//
//        //Dunas
//        g.setColor(Color.black);
//        g.fillArc(180, 730, 120, 50, 0, 220);
//        g.setColor(Color.white);
//        g.fillArc(180, 735, 120, 50, 0, 220);
//        g.setColor(Color.black);
//        g.fillArc(460, 730, 120, 50, 0, 220);
//        g.setColor(Color.white);
//        g.fillArc(460, 735, 120, 50, 0, 220);
//        g.setColor(Color.black);
//        g.fillArc(260, 780, 140, 50, 0, 220);
//        g.setColor(Color.white);
//        g.fillArc(260, 785, 140, 50, 0, 220);
//
//        //Circulo medio
//        g.setColor(Color.black);
//        g.fillOval(275, 335, 250, 250);
//        g.setColor(Color.white);
//        g.fillOval(279, 339, 242, 242);
//        g.setColor(Color.black);
//
//        //Sombrero
//        g.setColor(Color.black);
//        g.fillRoundRect(335, 100, 130, 200, 90, 90);
//        g.setColor(Color.white);
////        g.fillRoundRect(142, 85, 200, 130, 110, 110);
////        g.fillRoundRect(460, 85, 200, 130, 110, 110);
//
//        //Cabeza
//        g.setColor(Color.black);
//        g.fillOval(300, 195, 200, 200);
//        g.setColor(Color.white);
//        g.fillOval(304, 199, 192, 192);
//        g.setColor(Color.black);
//
//        //Boca
//        g.fillOval(390, 340, 20, 20);
//        g.fillOval(360, 335, 20, 20);
//        g.fillOval(420, 335, 20, 20);
//        g.fillOval(340, 320, 20, 20);
//        g.fillOval(440, 320, 20, 20);
//
//        //Nariz
//        g.setColor(Color.orange);
//        g.fillOval(383, 290, 34, 34);
//
//        //Ojos
//        g.setColor(Color.black);
//
//        g.fillOval(340, 240, 40, 45);
//        g.fillOval(420, 240, 40, 45);
//
//        //botones
//        g.setColor(Color.black);
//        g.fillOval(385, 410, 30, 30);
//        g.fillOval(385, 460, 30, 30);
//        g.fillOval(385, 510, 30, 30);
//
//        //Sombrero
//        g.setColor(Color.black);
//        g.fillRoundRect(335, 184, 130, 20, 10, 10);
//        g.setColor(Color.gray);
//        g.fillRoundRect(339, 188, 122, 12, 10, 10);
//
//        g.setColor(Color.black);
//        g.fillRoundRect(376, 170, 44, 44, 10, 10);
//        g.setColor(Color.yellow);
//        g.fillRoundRect(380, 174, 36, 36, 10, 10);
//
//        g.setColor(Color.black);
//        g.fillRoundRect(385, 180, 26, 26, 10, 10);
//        g.setColor(Color.gray);
//        g.fillRoundRect(389, 184, 18, 18, 10, 10);
//
//        g.setColor(Color.black);
//        g.fillRoundRect(275, 200, 250, 20, 10, 10);
//        g.setColor(Color.gray);
//        g.fillRoundRect(279, 204, 242, 12, 10, 10);
//
//        c1.pintar(g);
//        c2.pintar(g);
        //Guias
        g.setColor(Color.red);
        //         x1    y1   x2   y2
        g.drawLine(400, 0, 400, 1000);
        g.drawLine(0, 500, 800, 500);

    }

}
