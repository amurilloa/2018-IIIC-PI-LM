/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snakeutn;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class MiPanel extends JPanel implements KeyListener {

    private Snake culebra;
    private int ms;

    public MiPanel() {
        culebra = new Snake();
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        ms+=200;
        System.out.println(ms/1000);
        //Fondo negro
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getHeight());
        //Juego Snake
        culebra.pintar(g);
        culebra.mover(getWidth(), getHeight());
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(1200, 800);
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        //System.out.println("Typed - Code:" + ke.getKeyCode() + " Char: " + ke.getKeyChar());
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        //System.out.println("Pressed - Code:" + ke.getKeyCode() + " Char: " + ke.getKeyChar());
        culebra.cambioDir(ke.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        //System.out.println("Released - Code:" + ke.getKeyCode() + " Char: " + ke.getKeyChar());
    }
}
