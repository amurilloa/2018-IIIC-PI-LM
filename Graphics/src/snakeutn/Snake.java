/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snakeutn;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class Snake {

    private Bola manzana;
    private LinkedList<Bola> snake;
    private int dir;

    public Snake() {
        snake = new LinkedList<>();
        config();
    }

    private void config() {
        manzana = new Bola(Color.green, 120, 120);
        snake.add(new Bola(Color.red, 600, 400));
        snake.add(new Bola(Color.lightGray, 600, 440));
        snake.add(new Bola(Color.lightGray, 600, 480));
    }

    public void pintar(Graphics g) {
        manzana.pintar(g);
        for (Bola bola : snake) {
            bola.pintar(g);
        }
    }

    public void mover(int ancho, int alto) {
        Bola cabAnt = snake.getFirst();

        //if (cabAnt.getX() == manzana.getX() && cabAnt.getY() == manzana.getY()) {
        if(cabAnt.getBounds().intersects(manzana.getBounds())){
            System.out.println("Encima de la manzana");
            Main.vel-=10;
            int nX = ancho/cabAnt.TAM - 1;
            int nY = alto/cabAnt.TAM - 1;
            manzana.setX(((int) (Math.random() * nX)) * cabAnt.TAM);
            manzana.setY(((int) (Math.random() * nY)) * cabAnt.TAM);
        } else if (dir > 0) {
            snake.removeLast();
        }

        if (dir == 1) {
            Bola nue = new Bola(Color.red, cabAnt.getX() - cabAnt.TAM, cabAnt.getY());
            //snake.getFirst().setColor(Color.lightGray);
            cabAnt.setColor(Color.lightGray);
            snake.addFirst(nue);
        } else if (dir == 2) {
            Bola nue = new Bola(Color.red, cabAnt.getX(), cabAnt.getY() - cabAnt.TAM);
            cabAnt.setColor(Color.lightGray);
            snake.addFirst(nue);
        } else if (dir == 3) {
            Bola nue = new Bola(Color.red, cabAnt.getX() + cabAnt.TAM, cabAnt.getY());
            cabAnt.setColor(Color.lightGray);
            snake.addFirst(nue);
        } else if (dir == 4) {
            Bola nue = new Bola(Color.red, cabAnt.getX(), cabAnt.getY() + cabAnt.TAM);
            cabAnt.setColor(Color.lightGray);
            snake.addFirst(nue);
        }
    }

    public void cambioDir(int keyCode) {
        if (keyCode >= 37 && keyCode <= 40) {
            dir = keyCode - 36;
        }
    }

}
