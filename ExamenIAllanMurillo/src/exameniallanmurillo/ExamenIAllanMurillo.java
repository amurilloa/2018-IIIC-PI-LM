/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exameniallanmurillo;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class ExamenIAllanMurillo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logica log = new Logica();
        int[] arreglo = {5, 3, 3, 4, 2, 4, 34, 6, 2};
        System.out.println(log.imprimir(arreglo));

        String menu = "Menu Principal v0.1\n"
                + "i.   Imprimir números\n"
                + "ii.  Sumar Pares\n"
                + "iii. Cantidad Primos\n"
                + "iv.  Salir\n"
                + "Seleccione una opción: ";

        while (true) {
            String op = Util.leerString(menu);
            if ("i".equals(op)) {
                System.out.println(log.imprimir(log.crearArreglo(5, 10)));
                System.out.println(log.imprimir(log.crearArreglo(10, 5)));
            } else if ("ii".equals(op)) {
                System.out.println(log.sumarPares(3, arreglo));
            } else if ("iii".equals(op)) {
                System.out.println(log.contarPrimos(arreglo));
            } else if ("iv".equals(op)) {
                System.out.println("Gracias por utilizar la aplicación");
                break;
            }
        }

        Estudiante[] estudiantes = new Estudiante[10];
        estudiantes[0] = new Estudiante(1, "Allan", "Murillo", "Alfaro", "amurilloa@utn.ac.cr", "8526-2638");
        estudiantes[1] = new Estudiante(2, "Roberto", "Murillo", "Alfaro", "rmurilloa@utn.ac.cr", "2526-2638");
        estudiantes[2] = new Estudiante(3, "Monserrath", "Murillo", "Alfaro", "mmurilloa@utn.ac.cr", "6526-2638");

        for (Estudiante estudiante : estudiantes) {
            if (estudiante != null) {
                System.out.println(estudiante.obtenerNombre());
            }
        }

    }

}
