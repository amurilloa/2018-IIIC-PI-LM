/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exameniallanmurillo;

/**
 *
 * @author ALLAN
 */
public class Estudiante {

    private int cedula;
    private String nombre;
    private String apellidoUno;
    private String apellidoDos;
    private String email;
    private String telefono;

    public Estudiante() {
    }

    public Estudiante(int cedula, String nombre, String apellidoUno, String apellidoDos, String email, String telefono) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellidoUno = apellidoUno;
        this.apellidoDos = apellidoDos;
        this.email = email;
        this.telefono = telefono;
    }

    public String obtenerNombre() {
        return nombre + " " + apellidoUno + " " + apellidoDos;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoUno() {
        return apellidoUno;
    }

    public void setApellidoUno(String apellidoUno) {
        this.apellidoUno = apellidoUno;
    }

    public String getApellidoDos() {
        return apellidoDos;
    }

    public void setApellidoDos(String apellidoDos) {
        this.apellidoDos = apellidoDos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

}
