/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriotres.ciudades;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private Ciudad[] destinos;

    public Logica() {
        destinos = new Ciudad[10];
    }

    public void regDestinos(Ciudad ciudad) {
        for (int i = 0; i < destinos.length; i++) {
            if (destinos[i] == null) {
                destinos[i] = ciudad;
                break;
            }
        }
    }

    public String dondeIr(int monto) {
        String info = "";
        for (Ciudad c : destinos) {
            if (c != null && c.getTarifa() <= monto) {
                info += c.getNombre() + "(" + c.getTarifa() + ")\n";
            }
        }
        return "".equals(info) ? "Fondos insuficientes para viajar" : info;
    }

    public Ciudad masCara() {
        Ciudad mayor = destinos[0];
        for (Ciudad c : destinos) {
            if (c != null && c.getTarifa() > mayor.getTarifa()) {
                mayor = c;
            }
        }
        return mayor;
    }

}
