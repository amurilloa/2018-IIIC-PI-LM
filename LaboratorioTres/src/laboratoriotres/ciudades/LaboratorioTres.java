/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriotres.ciudades;

/**
 *
 * @author ALLAN
 */
public class LaboratorioTres {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logica log = new Logica();
        log.regDestinos(new Ciudad("San José", 1800));
        log.regDestinos(new Ciudad("Limón", 2800));
        log.regDestinos(new Ciudad("Ciudad Quesada", 1000));
        log.regDestinos(new Ciudad("Los Chiles", 2400));
        log.regDestinos(new Ciudad("San Ramón", 2000));

        int monto = 2000;
        //Calcular a cuales puedo ir con un monto
        System.out.println(log.dondeIr(monto));
        //Cual la mas cara
        System.out.println(log.masCara().getNombre());
        
        
    }

}
