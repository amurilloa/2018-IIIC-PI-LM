/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratoriotres.cajero;

/**
 *
 * @author ALLAN
 */
public class Cajero {

    private double saldo;
    private int[] billetes;
    private int[] dinero;

    public Cajero(double saldo) {
        this.saldo = saldo;
        billetes = new int[]{10000, 5000, 2000, 1000, 500, 100};
        dinero = new int[billetes.length];
    }

    public void deposito(double monto) {
        saldo += monto;
    }

    public boolean retiro(int monto) {
        if (monto <= saldo) {
            saldo -= monto;
            desgloseDinero(monto);
            return true;
        }
        return false;
    }

    private void desgloseDinero(int monto) {
        for (int i = 0; i < billetes.length; i++) {
            if (monto >= billetes[i]) {
                dinero[i] = monto / billetes[i];
                monto %= billetes[i];
            }
        }
    }

    public String tiquete(int monto) {
        String bi = imprimir(billetes);
        String di = imprimir(dinero);

        String str = String.format("Monto a retirar: %d\n"
                + "Vector de Billetes: %s\n"
                + "Vector de Dinero:   %s\n"
                + "Saldo Dispoble: %.2f", monto, bi, di, saldo);
        return str;
    }

    private String imprimir(int[] arreglo) {
        String str = "|";
        for (int dato : arreglo) {
            str += String.format("%-5s|", dato);
        }
        return str;
    }

    public double getSaldo() {
        return saldo;
    }
    
    
    

}
