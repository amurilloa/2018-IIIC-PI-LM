/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

import util.Util;

/**
 *
 * @author ALLAN
 */
public class Main {

    public static void main(String[] args) {
        Gato g = new Gato();
        while (true) {
            System.out.println(g.imprimir());
            int f = Util.leerInt("# Fila");
            int c = Util.leerInt("# Columna");
            if (!g.jugar(f, c)) {
                System.out.println("Campo no disponible, intente nuevamente");
            } else if (g.gano()) {
                System.out.println(g.imprimir());
                System.out.println("Felicidades.... gana el jugador " + g.ganador());
                break;
            } else if (g.empato()) {
                System.out.println(g.imprimir());
                System.out.println("Empato....");
                break;
            }
        }
    }
}
