/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

/**
 *
 * @author ALLAN
 */
public class Gato {

    private char[][] tablero;
    private boolean turno;

    public Gato() {
        tablero = new char[3][3];
        turno = true;
    }

    public String imprimir() {
        String str = "";
        for (int f = 0; f < tablero.length; f++) {
            for (int c = 0; c < tablero[f].length; c++) {
                str += (tablero[f][c] == '\u0000' ? '_' : tablero[f][c]) + " ";
            }
            str += "\b\n";
        }
        return str;
    }

    public boolean jugar(int f, int c) {
        f--;
        c--;
        if (tablero[f][c] == '\u0000') {
            tablero[f][c] = turno ? 'X' : 'O';
            turno = !turno;
            return true;
        }
        return false;
    }

    public boolean gano() {
        //Filas
        for (int f = 0; f < tablero.length; f++) {
            if (tablero[f][0] != '\u0000'
                    && tablero[f][0] == tablero[f][1]
                    && tablero[f][0] == tablero[f][2]) {
                return true;
            }
        }
        //Columnas
        for (int c = 0; c < tablero.length; c++) {
            if (tablero[0][c] != '\u0000'
                    && tablero[0][c] == tablero[1][c]
                    && tablero[0][c] == tablero[2][c]) {
                return true;
            }
        }
        //Diagonal 1 
        if (tablero[0][0] != '\u0000'
                && tablero[0][0] == tablero[1][1]
                && tablero[0][0] == tablero[2][2]) {
            return true;
        }
        //Diagonal 2 
        if (tablero[0][2] != '\u0000'
                && tablero[0][2] == tablero[1][1]
                && tablero[0][2] == tablero[2][0]) {
            return true;
        }
        return false;
    }

    public String ganador() {
        return turno ? "O" : "X";
    }

    public boolean empato() {
        for (int f = 0; f < tablero.length; f++) {
            for (int c = 0; c < tablero[f].length; c++) {
                if (tablero[f][c] == '\u0000') {
                    return false;
                }
            }
        }
        return true;
    }
}






