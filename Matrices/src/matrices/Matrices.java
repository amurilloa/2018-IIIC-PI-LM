/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrices;

/**
 *
 * @author ALLAN
 */
public class Matrices {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Logica log = new Logica(3, 3);
        log.llenar();
        System.out.println(log.imprimir());
        System.out.println(log.sumarDatos());
        System.out.println(log.contarImpares());
        System.out.println(log.promedioPar());
    }

    /*
     - Crear una logica(constructor filas y columnas), una matriz como atributo
     - Hacer un método de imprimir(imprime la matriz de la clase)
     - Llenar la matriz con datos aleatorios 1-10
     - Sumar los datos de la matriz
     - Contar la cantidad de impares
     - Promedio de números pares.
     */
}