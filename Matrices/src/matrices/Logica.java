/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrices;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private int[][] matriz;

    public Logica(int filas, int columnas) {
        matriz = new int[filas][columnas];
    }

    public String imprimir() {
        String res = "";
        for (int f = 0; f < matriz.length; f++) {
            for (int c = 0; c < matriz[f].length; c++) {
                res += matriz[f][c] + ", ";
            }
            res += "\b\b\n";
        }
        return res;
    }

    public void llenar() {
        for (int f = 0; f < matriz.length; f++) {
            for (int c = 0; c < matriz[f].length; c++) {
                matriz[f][c] = (int) (Math.random() * 9) + 1;
            }
        }
    }

    public int sumarDatos() {
        int total = 0;
        for (int f = 0; f < matriz.length; f++) {
            for (int c = 0; c < matriz[f].length; c++) {
                total += matriz[f][c];
            }
        }
        return total;
    }

    public int contarImpares() {
        int cantidad = 0;
        for (int f = 0; f < matriz.length; f++) {
            for (int c = 0; c < matriz[f].length; c++) {
                if (matriz[f][c] % 2 != 0) {
                    cantidad++;
                }
            }
        }
        return cantidad;
    }

    public double promedioPar() {
        int sum = 0;
        int can = 0;
        for (int f = 0; f < matriz.length; f++) {
            for (int c = 0; c < matriz[f].length; c++) {
                if (matriz[f][c] % 2 == 0) {
                    sum += matriz[f][c];
                    can++;
                }
            }
        }
        return (double) sum / can;
    }

}
