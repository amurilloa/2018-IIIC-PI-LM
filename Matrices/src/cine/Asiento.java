/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cine;

/**
 *
 * @author ALLAN
 */
public class Asiento {

    private int numero;
    private char letra;
    private int precio;
    private boolean disponible;

    public Asiento() {
    }

    public Asiento(int numero, char letra, int precio, boolean disponible) {
        this.numero = numero;
        this.letra = letra;
        this.precio = precio;
        this.disponible = disponible;
    }

    public String getNombre() {
        return String.valueOf(letra) + numero;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public char getLetra() {
        return letra;
    }

    public void setLetra(char letra) {
        this.letra = letra;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

}
