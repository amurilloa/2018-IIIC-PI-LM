/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cine;

/**
 *
 * @author ALLAN
 */
public class Cine {

    private Asiento[][] asientos;

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";

    public Cine() {
        asientos = new Asiento[9][9];
        construir();
    }

    public void construir() {
        for (int f = 0; f < asientos.length; f++) {
            int precio = f < 3 ? 3000 : f < 6 ? 4000 : 5000;
            for (int c = 0; c < asientos[f].length; c++) {
                asientos[f][c] = new Asiento(c + 1, (char) (65 + f), precio, true);
            }
        }
    }

    public boolean comprar(String nombre) {
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                if (asientos[f][c].getNombre().equals(nombre) && asientos[f][c].isDisponible()) {
                    asientos[f][c].setDisponible(false);
                    return true;
                }
            }
        }
        return false;
    }

    public String disponibilidad() {
        String info = "Tipo 1: %d/27\n"
                + "Tipo 2: %d/27\n"
                + "Tipo 3: %d/27\n"
                + " Total: %d/81";
        int p1 = 0;
        int p2 = 0;
        int p3 = 0;

        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                Asiento t = asientos[f][c];
                if (t.getPrecio() == 3000 && !t.isDisponible()) {
                    p1++;
                } else if (t.getPrecio() == 4000 && !t.isDisponible()) {
                    p2++;
                } else if (!t.isDisponible()) {
                    p3++;
                }
            }
        }

        return String.format(info, p1, p2, p3, (p1 + p2 + p3));
    }

    public String verAsientos() {
        String str = "";
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                if (asientos[f][c].isDisponible()) {
                    str += ANSI_GREEN;
                } else {
                    str += ANSI_RED;
                }
                str += asientos[f][c].getNombre() + " ";
                str += ANSI_RESET;
            }
            str += "\n";
        }
        return str;
    }

    public int taquilla() {
        int total = 0;
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                if (!asientos[f][c].isDisponible()) {
                    total += asientos[f][c].getPrecio();
                }
            }
        }
        return total;
    }
}
